# Fetches NASA's Astronomy Picture of the Day,
# captions it, and sets it as XFCE's wallpaper.

from bs4 import BeautifulSoup
import urllib.request
import subprocess
import os
from PIL import Image, ImageDraw, ImageFont

# Full path. Leave blank to use current directory.
image_dir = ''

# centered, tiled, stretched, scaled, zoomed (default)
wallpaper_behavior = 'zoomed'

# True, False. Center-crop to screen resolution.
resize_wallpaper = False

# True, False. Add NASA's caption to bottom right-hand corner.
caption = True

# True, False. Save GIFs (which might be animated).
save_gifs = False

# Run `xfconf-query -c xfce4-desktop -l` to find.
xfce_desktop_path = "/backdrop/screen0/monitorLVDS1/workspace0/last-image"
xfce_style_path = "/backdrop/screen0/monitorLVDS1/workspace0/image-style"


def convert_wallpaper_behavior(behavior):
    if behavior == "centered":
        return 1
    elif behavior == "tiled":
        return 2
    elif behavior == "stretched":
        return 3
    elif behavior == "scaled":
        return 4
    elif behavior == "zoomed":
        return 5
    else:
        return 5


def crop_to_center(image_file):
    main_image = Image.open(image_file)
    image_width, image_height = main_image.size
    resolution = get_screen_resolution()
    resolution_width, resolution_height = resolution

    if resolution_width > image_width:
        print("User's resolution larger than image. Skipping resize.")
        return
    else:
        main_image = main_image.crop( \
            box=((image_width-resolution_width)/2, \
            (image_height-resolution_height)/2, \
            (image_width+resolution_width)/2, \
            (image_height+resolution_height)/2) \
        )
        main_image.save(image_file)
        print("Wallpaper resized to {}".format(resolution))


def get_screen_resolution():
    res = subprocess.check_output("xrandr \
        | grep \* | awk '{print $1}'", shell=True)
    res = res[:-1].decode().split("x") # Strip \n and convert to string.
    res = (int(res[0]), int(res[1]))
    return res


def overlay_caption(image_file, title):
    main_image = Image.open(image_file).convert("RGBA")
    transparency = Image.new('RGBA', main_image.size, (0,0,0,0))
    image_width, image_height = main_image.size

    fnt = ImageFont.truetype('Pillow/Tests/fonts/DejaVuSans.ttf', 24)
    text_width, text_height = fnt.getsize(title)

    draw = ImageDraw.Draw(transparency)
    draw.rectangle( \
        [(image_width-text_width-25, \
        image_height-text_height-35), \
        (image_width, \
        image_height)], \
        fill=(0,0,0,128), \
        outline=None \
    )
    draw.text( \
        (image_width-text_width-10, \
        image_height-text_height-20), \
        title, \
        font=fnt, \
        fill=(255,255,255,255)\
    )

    out = Image.alpha_composite(main_image,transparency)
    out.convert('RGB').save(image_file)


def main():
    potd_url = 'https://apod.nasa.gov/apod/astropix.html'

    try:
        page = urllib.request.urlopen(potd_url)
    except:
        print("The POTD URL wasn't found.")
        exit(1)

    soup = BeautifulSoup(page, 'html.parser')

    try:
        potd_image = 'https://apod.nasa.gov/apod/' + \
            soup.find_all('img')[0].find_parent('a')['href']
    except IndexError:
        print("Sorry, no picture was available. Maybe it was a video today.")
        exit(1)

    # See if the image is a GIF.
    if os.path.splitext(potd_image)[1] == ".gif" and not save_gifs:
        print("Image is a GIF. Skipping. Set save_gifs to True to save anyway.")
        exit(1)

    # If the user set the image directory, use that. Else, get the system path.
    if image_dir:
        full_path = image_dir + "/" + os.path.basename(potd_image)
    else:
        full_path = os.getcwd() + "/" + os.path.basename(potd_image)

    # Save image.
    try:
        urllib.request.urlretrieve(potd_image, full_path)
        print("{} saved".format(full_path))
    except:
        print("A high-res image wasn't found. Exiting.")
        exit(1)

    # Crop image to user's resolution and save.
    if resize_wallpaper:
        crop_to_center(full_path)

    # Add caption to image and save.
    if caption:
        try:
            potd_caption = soup.find_all('center')[1].b.string
        except IndexError:
            print("Couldn't find a caption. Continuing without.")

        overlay_caption(full_path, potd_caption)
        print("Wallpaper captioned with: {}".format(potd_caption))

    # Set image as background and adjust wallpaper behavior.
    try:
        os.system("xfconf-query -c xfce4-desktop -p {} \
            -s {}".format(xfce_desktop_path, full_path))
        os.system("xfconf-query -c xfce4-desktop -p {} \
            -s {}".format(xfce_style_path, convert_wallpaper_behavior(wallpaper_behavior)))
        print("{} set as background".format(full_path))
    except:
        print("Couldn't set wallpaper. Check your xfconf-query paths.")
        exit(1)

if __name__ == '__main__':
    main()
